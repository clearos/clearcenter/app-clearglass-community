<?php

/**
 * ClearGLASS Community controller.
 *
 * @category   apps
 * @package    clearglass-community
 * @subpackage controllers
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.clearcenter.com/app_license ClearCenter license
 * @link       http://www.clearcenter.com/support/documentation/clearos/clearglass-community/
 */

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ClearGLASS Community controller.
 *
 * @category   apps
 * @package    clearglass-community
 * @subpackage controllers
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.clearcenter.com/app_license ClearCenter license
 * @link       http://www.clearcenter.com/support/documentation/clearos/clearglass-community/
 */

class ClearGLASS_Community extends ClearOS_Controller
{
    /**
     * ClearGLASS summary view.
     *
     * @return view
     */

    function index()
    {
        // Show Certificate Manager widget if it is not initialized
        //---------------------------------------------------------

        $this->load->module('certificate_manager/certificate_status');

        if (! $this->certificate_status->is_initialized()) {
            $this->certificate_status->widget();
            return;
        }

        // Load libraries
        //---------------

        $this->load->library('clearglass/ClearGLASS');
        $this->load->library('clearglass_community/ClearGLASS_Community');
        $this->lang->load('clearglass_community');

        // Load view data
        //---------------

        try {
            $is_capable = $this->clearglass->is_capable();
            $is_initialized = $this->clearglass->is_initialized();
            $is_installed = $this->clearglass_community->is_installed();
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }
        // Load views
        //-----------

        if (!$is_capable) {
            redirect('/clearglass_community/settings/limited');
        } else if (!$is_initialized) {
            redirect('/clearglass_community/settings/edit/init');
        } else if (!$is_installed) {
            redirect('/clearglass_community/install/index');
        } else {
            $views = array(
                'clearglass_community/project',
                'clearglass_community/network',
                'clearglass_community/info',
                'clearglass_community/settings',
                'clearglass_community/containers'
            );

            $this->page->view_forms($views, lang('clearglass_community_app_name'));
        }
    }
}
