
Name: app-clearglass-community
Epoch: 1
Version: 2.5.46
Release: 1%{dist}
Summary: ClearGLASS Community
License: GPLv3
Group: Applications/Apps
Packager: ClearCenter
Vendor: ClearCenter
Source: %{name}-%{version}.tar.gz
Buildarch: noarch
Requires: %{name}-core = 1:%{version}-%{release}
Requires: app-base
Requires: app-network
Requires: app-certificate-manager
Requires: app-clearglass

%description
ClearGLASS is a single dashboard to manage multi-cloud infrastructure.

%package core
Summary: ClearGLASS Community - API
License: LGPLv3
Group: Applications/API
Requires: app-base-core
Requires: app-clearglass-core >= 1:2.5.20
Requires: clearglass-community >= 3.3.0
Requires: app-docker-core >= 1:2.5.20

%description core
ClearGLASS is a single dashboard to manage multi-cloud infrastructure.

This package provides the core API and libraries.

%prep
%setup -q
%build

%install
mkdir -p -m 755 %{buildroot}/usr/clearos/apps/clearglass_community
cp -r * %{buildroot}/usr/clearos/apps/clearglass_community/

install -d -m 0755 %{buildroot}/var/clearos/clearglass_community
install -d -m 0755 %{buildroot}/var/clearos/clearglass_community/backup
install -D -m 0644 packaging/clearglass-community.php %{buildroot}/var/clearos/docker/project/clearglass-community.php

%post
logger -p local6.notice -t installer 'app-clearglass-community - installing'

%post core
logger -p local6.notice -t installer 'app-clearglass-community-core - installing'

if [ $1 -eq 1 ]; then
    [ -x /usr/clearos/apps/clearglass_community/deploy/install ] && /usr/clearos/apps/clearglass_community/deploy/install
fi

[ -x /usr/clearos/apps/clearglass_community/deploy/upgrade ] && /usr/clearos/apps/clearglass_community/deploy/upgrade

exit 0

%preun
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-clearglass-community - uninstalling'
fi

%preun core
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-clearglass-community-core - uninstalling'
    [ -x /usr/clearos/apps/clearglass_community/deploy/uninstall ] && /usr/clearos/apps/clearglass_community/deploy/uninstall
fi

exit 0

%files
%defattr(-,root,root)
/usr/clearos/apps/clearglass_community/controllers
/usr/clearos/apps/clearglass_community/htdocs

%files core
%defattr(-,root,root)
%exclude /usr/clearos/apps/clearglass_community/packaging
%exclude /usr/clearos/apps/clearglass_community/unify.json
%dir /usr/clearos/apps/clearglass_community
%dir /var/clearos/clearglass_community
%dir /var/clearos/clearglass_community/backup
/usr/clearos/apps/clearglass_community/deploy
/usr/clearos/apps/clearglass_community/language
/usr/clearos/apps/clearglass_community/libraries
/var/clearos/docker/project/clearglass-community.php
